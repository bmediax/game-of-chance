// 'Possible Answers' Object
let pos_answers = {
    affirm: {
        color: '#5CC875',
        darkColor: '#5C6575',
        prompt: [
            'It is Certain', 
            'It is decidedly so.', 
            'Without a doubt.', 
            'Yes - definitely.', 
            'You may rely on it.', 
            'As I see it, yes.', 
            'Most likely.', 
            'Outlook good.', 
            'Yes.', 
            'Signs point to yes.'
        ]
    },

    nonCommit: {
        color: '#EBDC57',
        darkColor: '#D6874F',
        prompt: [
            'Reply hazy, try again.',
            'Ask again later.',
            'Better not tell you now.',
            'Cannot predict now.',
            'Concentrate and ask again.'
        ]
    },

    negative: {
        color: '#FF675A',
        darkColor: '#AB6659',
        prompt: [
            "Don't count on it.",
            'My reply is no.',
            'My sources say no.',
            'Outlook is not good.',
            'Very doubtful'
        ]   
    }
}

// 20 Possible Answers
document.getElementById('shakeBall').addEventListener('click', function() {
    let input = document.getElementById('answerInput').value

    if ( input.length < 1 ) {
        alert("Please enter a question")
    } 
    
    else {
        let randNum = Math.floor(Math.random() * 3)
        let numOutput = document.getElementById('num_output')
        let inputCirc = document.getElementById('ball-object')
        let background = document.body

        // Switching through Random_Number (randNum)
        switch (randNum) {
            case 0:
                let affirmPrompt = pos_answers.affirm.prompt[Math.floor(Math.random()*pos_answers.affirm.prompt.length)]
                numOutput.innerHTML = affirmPrompt
                inputCirc.style.backgroundColor = pos_answers.affirm.color
                background.style.backgroundColor = pos_answers.affirm.darkColor
                break
            case 1:
                let nonCommitPrompt = pos_answers.nonCommit.prompt[Math.floor(Math.random()*pos_answers.nonCommit.prompt.length)]
                inputCirc.style.backgroundColor = pos_answers.nonCommit.color
                background.style.backgroundColor = pos_answers.nonCommit.darkColor
                numOutput.innerHTML = nonCommitPrompt
                break
            case 2:
                let negativePrompt = pos_answers.negative.prompt[Math.floor(Math.random()*pos_answers.negative.prompt.length)]
                inputCirc.style.backgroundColor = pos_answers.negative.color
                background.style.backgroundColor = pos_answers.negative.darkColor
                numOutput.innerHTML = negativePrompt
                break
            default:
                console.log('Try Again!')
        }
    }
})

// Reloads the Browser
document.getElementById('reset').addEventListener('click', function() {
    location.reload(true)
})